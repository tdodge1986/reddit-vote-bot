const snoowrap = require('snoowrap');
const cred = require('./credentials/credentials.json')


const r = new snoowrap({
  userAgent: cred.userAgent,
  clientId: cred.clientId,
  clientSecret: cred.clientSecret,
  username: cred.username,
  password: cred.password
});

function theVote(id) {
  r.getSubmission(id).downvote()
}

theVote('6ba4a2');
